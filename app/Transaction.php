<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [

        'order_id', 
        'staff_id',
        'package_id',
        'member_name',
        'customer_id',
        'order_status',
        'total',
        'amount_received',
        'comment'

    ];
}
