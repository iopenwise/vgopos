<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Transaction;
use App\Order;
use App\Package;
use App\User;
use PDF;
use DB;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Order::orderBy('created_at', 'desc')->get()->toArray();
        return view ('transaction.index', compact('transactions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //p
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order_ids = json_decode($_POST['order_ids'], TRUE);
        $package_ids = json_decode($_POST['package_ids'], TRUE);
        $member = json_decode($_POST['member'], TRUE);

        $order_id = implode(',',$order_ids);
        $package_id = implode(',',$package_ids);
        $member = implode(',',$member);

        $transaction = new Transaction([

            'order_id'          =>          $order_id, //done
            'staff_id'          =>          $request->staff_id, //done
            'package_id'        =>          $package_id, //done
            'member_name'       =>          $member, //done
            'customer_id'       =>          $request->customer_id, //done
            'order_status'      =>          'Paid',
            'total'             =>          $request->total_price,
            'amount_received'   =>          $request->add_payment,
            'comment'           =>          $request->comments



        ]);

       //dd($request->all());


        $transaction->save();
        session()->flash('flash_message', 'New Transaction Added');

        foreach($order_ids as $value){
          $id = $value;
          $amount_paid = $_POST['add_payment'];
          $payment_type = $_POST['payment_type'];
          $pesanan = Order::findOrFail($id);
          $staus_change= "Paid";
          $pesanan->update([
              'status'              =>        $staus_change,
          ]);
          $pesanan->save();
          $invoice_number = DB::select("SELECT invoice_number FROM `orders` WHERE invoice_number is NOT NULL order by invoice_number desc limit 1");
          // echo '<pre>';
          // print_r($invoice_number);
          // exit;
          if(empty($invoice_number)){
            $inv_no = "0000000001";
          } else {
            $inv_no = (int)$invoice_number[0]->invoice_number;
            $inv_no++;
            $str_length = 10;
            $inv_no = substr("0000000000{$inv_no}", -$str_length);
          }

          DB::table('orders')
              ->where('id', $id)
              ->update(['amount_paid' => "$amount_paid",'payment_method'=>"$payment_type","invoice_number"=>"$inv_no"]);
        }





        //return redirect('brand');
        if($_POST['payment_type'] == 'Cash'){
          $returnchange = $_POST['add_payment'] - $_POST['grand_total'];
          echo "<script>window.location.href='/transaction'</script>";
        } else {
          echo "<script>window.location.href='/transaction'</script>";
        }

        //return redirect('transaction');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function downloadPDF($id)

    {

        // $transactions = Transaction::find($id);
        // $order_id = $transactions->order_id;
        // $packages = Package::all()->toArray();
        $users = User::all()->toArray();

        // $order = Order::find($order_id);

        $orders = DB::select("SELECT * from orders where `id` = $id");
        $package_id = $orders[0]->package_id;
        $packages = DB::select("SELECT * from packages where `id` = $package_id");


        $filename = 'Receipt'.'.'.'pdf';

        $pdf = PDF::loadView('receipt', compact('orders','transactions','packages','users'));
        return $pdf->stream($filename);
    }
}
