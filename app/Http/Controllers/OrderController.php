<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Order;
use App\Orders;
use App\Package;
use App\User;
use DB;
use Excel;
use Auth;

class OrderController extends Controller
{
    public function index(){

        $orderhistory = Order::all()->toArray();
        $packages = Package::all()->toArray();
        $user_name = User::all()->toArray();

        $orders = DB::select("SELECT distinct(cust_id) from orders where `status` = 'Pending'");

        if(empty($orders)){
          $empty = 1;
          $customer[] = array();
        } else {
          foreach ($orders as $value) {
            $cust = DB::select("SELECT id,name from users where id=$value->cust_id");
            if(!empty($cust)){
              $customer[] = $cust;
              $empty = 0;
            } else {
              $empty = 1;
            }
          }
        }

        // return view('order.index' , compact('orderhistory', 'packages','user_name', 'orders'));
        return view('order.index' , compact('customer','empty'));
    }




    public function showList(){
      $id = $_POST['customer'];
      $orderhistory = DB::select("SELECT * from orders where `status` = 'Pending' and cust_id=$id");
      $packages = Package::all()->toArray();
      $user_name = User::all()->toArray();


      return view('order.index2' , compact('orderhistory', 'packages','user_name', 'orders'));
    }

    public function report(){
      return view('order.report');
    }

    public function showReport(){
      $payment_status = $_POST['payment_status'];
      $payment_type = $_POST['payment_type'];

      $daterange = explode('-', $_POST['daterange']);
      $daterange1 = preg_replace('/\s+/', '',$daterange[0]);
      $daterange2 = preg_replace('/\s+/', '',$daterange[1]);

      $date1 = str_replace('/', '-', $daterange1);
      $date2 = str_replace('/', '-', $daterange2);

      $date3 = date('Y-m-d', strtotime($date1));
      $date4 = date('Y-m-d', strtotime($date2));
      if($payment_status == 'Pending'){
        $orders = DB::select("select * from orders where status = '".$payment_status."' and updated_at between '".$date3."' and '".$date4."' order by id desc;");
      } else {
        $orders = DB::select("select * from orders where payment_method = '".$payment_type."' and status = '".$payment_status."' and updated_at between '".$date3."' and '".$date4."' order by id desc;");
      }

      return view('order.showreport' , compact('orders', 'payment_status', 'payment_type', 'date3', 'date4'));
    }

    public function downloadreport(){
      $payment_status = $_POST['payment_status'];
      $payment_type = $_POST['payment_type'];
      $date1 = $_POST['date1'];
      $date2 = $_POST['date2'];

      if($payment_status == 'Pending'){
        $orders = DB::select("select id,member_name,cust_id,package_id,package_price,package_depo,status,package_total,amount_paid,payment_method,invoice_number,updated_at from orders where status = '".$payment_status."' and updated_at between '".$date1."' and '".$date2."' order by id desc;");
        //$orders = Orders::where('status','=',"$payment_status")->where('updated_at','between',)
      } else {
        $orders = DB::select("select id,member_name,cust_id,package_id,package_price,package_depo,status,package_total,amount_paid,payment_method,invoice_number,updated_at from orders where payment_method = '".$payment_type."' and status = '".$payment_status."' and updated_at between '".$date1."' and '".$date2."' order by id desc;");
      }

      $array = json_decode(json_encode($orders), true);

      Excel::create("$date1 to $date2", function($excel) use($array) {
          $excel->sheet('ExportFile', function($sheet) use($array) {
              $sheet->fromArray($array);
          });
      })->export('xls');
    }

}
