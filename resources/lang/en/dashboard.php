<?php

return [

	'welcome' 			=> 'Welcome to VGO POS System.',
	'statistics' 		=> 'Statistics',
	'total_employees' 	=> 'Total Employees',
	'total_customers' 	=> 'Total Customers',
	'total_suppliers' 	=> 'Total Suppliers',
	'total_package' 		=> 'Total Package',
	'total_item_kits' 	=> 'Total Item Kits',
	'total_receivings' 	=> 'Total Receivings',
	'total_sales' 		=> 'Total Sales',

];
