<?php

return [

	'sales_register' => 'Sales Register',
	'search_item' => 'Search Package:',
	'invoice' => 'Invoice',
	'employee' => 'Staff',
	'payment_type' => 'Payment Type',
	'customer' => 'Customer',
	'member' => 'Member',
	'item_id' => 'Package ID',
	'item_name' => 'Package Name',
	'price' => 'Price',
	'deposit' => 'Deposit',
	'quantity' => 'Quantity',
	'total' => 'Total',
	'add_payment' => 'Add Payment',
	'comments' => 'Comments',
	'grand_total' => 'TOTAL:',
	'amount_due' => 'Amount Due',
	'submit' => 'Complete Sale',
	//struk
	'sale_id' => 'Sale ID',
	'item' => 'Package',
	'price' => 'Price',
	'qty' => 'Qty',
	'print' => 'Print',
	'new_sale' => 'New Sale',

];
