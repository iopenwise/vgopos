<?php

return [

	'list_items'	=> 'List Of Package',
	'new_item'		=> 'New Package',
	'item_id' 		=> 'Package ID',
	'upc_ean_isbn'	=> 'UPC/EAN/ISBN',
	'item_name' 	=> 'Package Name',
	'size' 			=> 'Size',
	'cost_price' 	=> 'Package Price',
	'selling_price' => 'Deposit Price',
	'quantity'		=> 'Quantity',
	'avatar'		=> 'Avatar',
	'edit' => 'Edit',
	'delete' => 'Delete',
	'submit' => 'Submit',
	'description' => 'Description',
	'choose_avatar' => 'Choose Avatar',
	'inventory' => 'Inventory',
	'update_item' => 'Update Item',
	//inventory
	'inventory_data_tracking' => 'Inventory Data Tracking',
	'current_quantity' => 'Current Quantity',
	'inventory_to_add_subtract' => 'Inventory to add/subtract',
	'comments' => 'Comments',
	'employee' => 'Employee',
	'in_out_qty' => 'In/Out Qty',
	'remarks' => 'Remarks',

];
