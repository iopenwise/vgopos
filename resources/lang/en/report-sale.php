<?php

return [

	'reports' => 'Reports',
	'sales_report' => 'Sales Report',
	'grand_total' => 'TOTAL',
	'grand_profit' => 'PROFIT',
	'sale_id' => 'Sale ID',
	'date' => 'Date',
	'items_purchased' => 'Package Purchased',
	'sold_by' => 'Sold By',
	'sold_to' => 'Sold To',
	'total' => 'Total',
	'profit' => 'Profit',
	'payment_type' => 'Payment Type',
	'comments' => 'Comments',
	'detail' => 'Detail',
	'item_id' => 'Package ID',
	'item_name' => 'Package Name:',
	'quantity_purchase' => 'Duration Of Subscription',

];
