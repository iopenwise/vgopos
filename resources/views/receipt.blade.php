<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!-- <p style="text-align: right;"><strong>VGO INFINITY SDN. BHD.</strong></p>
<p style="text-align: right;">1222524-K</p>
<p style="text-align: right;">Jalan Persiaran Golf,</p>
<p style="text-align: right;">Off Jalan Jumbo,</p>
<p style="text-align: right;">81250 Senai, Johor.</p>
<p style="text-align: right;">Tel: 016-778 5571</p>
<p style="text-align: right;">1222524-K</p> -->
<body>
<div style="float: left;" class="col-md-6">
  <?php $image  = public_path() . '/images/vgo_logo.png'; ?>
  <!-- <img src="{{$image}}"> -->
</div>
<div style="float: right;" class="col-md-6">
  <p><strong>VGO AQUATIC CENTRE</strong></p>
    13A, Jalan Perindustrian Desa Aman 1B,<br>
    Industri Desa Aman,<br>
    Kepong, Kuala Lumpur<br>
  </p>
</div>
<div class="col-md-10" style="margin-top:200px; border-top: 2px solid black;">
  <table style="width:100%">
    <th style="font-size:20px;">
      <strong>TAX INVOICE</strong>
    </th>
  </table>

  <table style="font-size: 18px;">
    <tr>
      <td>Tax Inv No</td>
      <td>{{  $orders[0]->invoice_number }}</td>
    </tr>
    <tr>
      <td>Cashier</td>
      <td>Neel</td>
    </tr>
    <tr>
      <td>Customer ID</td>
      <td>654987</td>
    </tr>
    <tr>
      <td>Customer Name &nbsp;</td>
      <td>{{  $orders[0]->member_name }}</td>
    </tr>
    <tr>
      <td>Paid By</td>
      <td>{{$orders[0]->payment_method}}</td>
    </tr>
  </table>

  <table style="width:100%; margin-top:20px; font-size:18px; ">
    <tr>
      <th style="border-top: 1px solid black; border-bottom: 1px solid black; padding:2px;">PRODUCT</th>
      <th style="border-top: 1px solid black; border-bottom: 1px solid black; padding:2px;">TOTAL</th>
    </tr>
    <?php
      $price = 0;
    ?>
    @foreach($packages as $package)
    <?php $package_total = $package->price + $package->deposit; ?>
      <tr>
        <td style="padding-bottom:20px;">{{$package->name}}</td>
        <td style="padding-bottom:20px;">RM {{number_format($package_total,2) }}</td>
      </tr>
      <?php $price += $package_total; ?>
    @endforeach
    <tr>
      <td style="border-top: 1px solid black; padding-top:20px;">Total</td>
      <td style="border-top: 1px solid black; padding-top:20px;">{{number_format($price,2)}}</td>
    </tr>
    <tr>
      <td >Discount</td>
      <td>0.00</td>
    </tr>
    <tr>
      <td>Sub Total</td>
      <td>{{number_format($price,2)}}</td>
    </tr>
    <tr>
      <td style="padding-bottom:20px;">GST (0%)</td>
      <td style="padding-bottom:20px;">0.00</td>
    </tr>
    <tr>
      <td style="border-top: 1px solid black; padding-top:20px;"><strong>Grand Total</strong></td>
      <td style="border-top: 1px solid black; padding-top:20px;"><strong>{{number_format($price,2)}}</strong></td>
    </tr>
    <tr>
      <td>Paid Amount</td>
      <td>{{number_format($orders[0]->amount_paid,2)}}</td>
    </tr>
    <tr>
      <?php $returnchange = (int)$orders[0]->amount_paid - (int)$price; ?>
      <td>Return Change</td>
      <td>{{number_format($returnchange,2)}}</td>
    </tr>
  </table>
</div>
</body>
