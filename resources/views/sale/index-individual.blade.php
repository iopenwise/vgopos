@extends('app')
@section('content')
{!! Html::script('js/angular.min.js', array('type' => 'text/javascript')) !!}
{!! Html::script('js/sale.js', array('type' => 'text/javascript')) !!}

<div class="container-fluid">
   <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><span class="glyphicon glyphicon-inbox" aria-hidden="true"></span> Sales Register Test</div>

            <div class="panel-body">

                <form action="/transaction" method="POST"  enctype="multipart/form-data">

                    {!! csrf_field() !!}



                <div class="row" ng-controller="SearchItemCtrl">

                    <div class="col-md-9">

                        <div class="row">


                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="invoice" class="col-sm-3 control-label">{{trans('sale.invoice')}}</label>
                                        <div class="col-sm-9">
                                        <input type="text" class="form-control" id="invoice" value="Invoice" readonly/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="employee" class="col-sm-3 control-label">{{trans('sale.employee')}}</label>
                                        <div class="col-sm-9">
                                        <input type="text" class="form-control" id="employee" name="employee" value="{{ Auth::user()->name }}" readonly/>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="customer_id" class="col-sm-4 control-label">{{trans('sale.customer')}}</label>
                                        <div class="col-sm-8">
                                          @foreach($user_name as $user)
                                            @if($orders[0]['cust_id'] == $user['id'])
                                              <input type="text" class="form-control" id="customer" value="{{$user['name']}}" readonly/>
                                            @endif
                                          @endforeach
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label for="payment_type" class="col-sm-4 control-label">{{trans('sale.payment_type')}}</label>
                                        <div class="col-sm-8">
                                        {!! Form::select('payment_type', array('Cash' => 'Cash', 'Cheque' => 'Cheque', 'Debit Card' => 'Debit Card', 'Credit Card' => 'Credit Card'), Input::old('payment_type'), array('class' => 'form-control')) !!}
                                        </div>
                                    </div>
                                </div>

                        </div>

                        <table class="table table-bordered">
                            <tr>
                                <th>No:</th>
                                <th>Order ID</th>
                                <th>Package Name</th>
                                <th>Customer Name</th>
                                <th>Package Price</th>
                                <th>Package Deposit</th>
                                <th>Order Status</th>
                                <th>TOTAL</th>
                            </tr>
                            <?php
                              $i = 1;
                              $price = 0;
                             ?>


                            @foreach($orders as $order)
                              <tr>
                                <td>{{$i}}</td>
                                <td>{{$order['id']}}</td>
                                @foreach($packages as $package)
                                  @if($order['package_id'] == $package['id'])
                                    <td>{{$package['name']}}</td>
                                  @endif
                                @endforeach
                                @foreach($user_name as $user)
                                  @if($order['cust_id'] == $user['id'])
                                    <td>{{$user['name']}}</td>
                                  @endif
                                @endforeach
                                <td>{{$order['package_price']}}</td>
                                <td>{{$order['package_depo']}}</td>
                                <td>{{$order['status']}}</td>
                                <td><strong>{{$order['package_total']}}</strong></td>
                              </tr>
                              <?php $i++; ?>
                              <?php
                                $price += $order['package_total'];
                                $package_id[] = $order['package_id'];
                                $order_id[] = $order['id'];
                                $member[] = $order['member_name'];
                              ?>

                            @endforeach

                        </table>


                        <?php
                          $order_id = json_encode($order_id);
                          $package_id = json_encode($package_id);
                          $member = json_encode($member);
                         ?>
                        <input type="hidden" name="order_ids" value="{{$order_id}}" />
                        <input type="hidden" name="package_ids" value="{{$package_id}}" />
                        <input type="hidden" name="member" value="{{$member}}" />
                        <input type="hidden" id="staff_id" name="staff_id" value="{{ Auth::id() }}">
                        <input type="hidden" id="customer_id" name="customer_id" value="{{ $orders[0]['cust_id'] }}">
                        <input type="hidden" id="total_price" name="total_price" value="{{ $price }}">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="total" class="col-sm-4 control-label">{{trans('sale.add_payment')}}</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <div class="input-group-addon">RM</div>
                                            <input type="text" class="form-control" id="add_payment" name="add_payment"/>
                                        </div>
                                    </div>
                                </div>
                                <div>&nbsp;</div>
                                <div class="form-group">
                                    <label for="employee" class="col-sm-4 control-label">{{trans('sale.comments')}}</label>
                                    <div class="col-sm-8">
                                    <input type="text" class="form-control" name="comments" id="comments" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="supplier_id" class="col-sm-4 control-label">{{trans('sale.grand_total')}}</label>
                                    <div class="col-sm-8">
                                    <div class="input-group">
                                        <div class="input-group-addon">RM</div>
                                        <input type="text" class="form-control" name="grand_total" id="grand_total" value="{{$price}}" readonly="true" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" style="margin-top:20px;">
                                <div class="form-group">
                                    <label for="supplier_id" class="col-sm-4 control-label">GST (0%)</label>
                                    <div class="col-sm-8">
                                      <div class="input-group">
                                          <div class="input-group-addon">RM</div>
                                          <input type="text" class="form-control" value="0.00" readonly="true" />
                                      </div>
                                    </div>
                                </div>
                            </div>

                                <div class="form-group">
                                    <div class="col-md-6" style="margin-top:20px;">
                                        <button type="button" id="submit_button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" disabled>Submit Payment</button>
                                    </div>
                                </div>
                          </div>



                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">

                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Payment Summary</h4>
                        </div>
                        <div class="modal-body">
                          <table class="table table-bordered" id="table">

                          </table>
                        </div>
                        <div class="modal-footer">
                          <button type="submit" class="btn btn-info btn-lg">Confirm</button>
                          <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal">Close</button>
                        </div>
                      </div>

                    </div>
                  </div>




                </form>
                    </div>

                </div>

            </div>
            </div>
        </div>
    </div>
</div>

<script>

$("#submit_button").on("click", function(){
  var grand_total = $("#grand_total").val();
  var add_payment = $("#add_payment").val();
  var gst = 0;
  var return_change = add_payment - grand_total;
  var table = "<tr><td>Total</td><td>RM "+grand_total+"</td></tr><tr><td>GST</td><td>RM "+gst+"</td></tr><tr><td>Amount Paid</td><td>RM "+add_payment+"</td></tr><tr><td>Return Change</td><td>RM "+return_change+"</td></tr>";
  $("#table").html(table);
})

  $('#add_payment').on("keyup", function(){
    var value = $('[name="payment_type"]').val();
    var add_payment = parseInt($(this).val());
    var grand_total = parseInt($('#grand_total').val());
    if(value != "Cash"){
      if(grand_total != add_payment){
        alert("Please enter exact amount");
        $('#submit_button').prop('disabled', true);
      } else {
        $('#submit_button').prop('disabled', false);
      }
    } else {
      if(grand_total <= add_payment){
        $('#submit_button').prop('disabled', false);
      } else {
        $('#submit_button').prop('disabled', true);
      }
    }
  })
</script>

@endsection
