<p style="text-align: center;">&nbsp;</p>
<p style="text-align: center;">&nbsp;</p>
<p style="text-align: center;"><strong>OFFICIAL RECEIPT</strong></p>
<p style="text-align: left;"><strong>ORDER ID : {{  $transactions->order_id }}</strong></p>
@foreach($users as $user)
@if($transactions['customer_id'] == $user['id'])
<p style="text-align: left;">Customer Name : <span style="color: #000080;"><strong>{{$user['name']}}</strong></span></p>
@endif
@endforeach
{{-- <p style="text-align: left;">Phone Number : <span style="color: #000080;"><strong>{{  $address->phone }} </strong></span></p> --}}
<p style="text-align: left;">Payment Method : <span style="color: #000080;"><strong></strong></span></p>
{{-- <p style="text-align: left;">Postage Address :&nbsp; <span style="color: #000080;"><br><strong>
                                              {{$address->street}} , <br>
                                              {{$address->postcode}} , {{$address->city}}<br>
                                              {{$address->state}}, <br> 
                                              {{$address->country}}</strong></span></p> --}}
<hr />
<p style="text-align: left;">&nbsp;</p>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">No</th>
      <th scope="col">Package Name</th>
      <th scope="col">Member Name</th>
      <th scope="col">Package Price</th>
      <th scope="col">Package Deposit</th>
      <th scope="col">Total Amount</th>
      <th scope="col">Amount Received</th>
      

    </tr>
  </thead>

  <tbody>
    <?php $i = 0 ?>
    
    <?php $i++ ?>

    <tr>
      <th scope="row">{{ $i}}</th>
      @foreach($packages as $package)
      @if($transactions['package_id'] == $package['id'])	 
	  <td>{{  $package['name']}}</td>
	  @endif
	  @endforeach
      <td>{{  $transactions->member_name }}</td>
      @foreach($packages as $package)
      @if($transactions['package_id'] == $package['id'])
      <td>RM {{  $package['price'] }}</td>
      <td>RM {{  $package['deposit'] }}</td>
      @endif
      @endforeach
      <td>RM {{  $transactions->total }}</td>
      <td>{{  $transactions->amount_received }}</td>
    </tr>

    


  </tbody>
</table>
<hr />








<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>