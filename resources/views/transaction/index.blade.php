@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default" style="width:120%;">
				<div class="panel-heading">List Of Transaction</div>

				<div class="panel-body">

<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <td>Order ID</td>
            <td>Date/Time</td>
            <td>Package Purchased</td>
            <td>Member Name</td>
            <td>Order Status</td>
            <td>Total Amount</td>
            <td>Print</td>

        </tr>
    </thead>
    <tbody>
        @foreach($transactions as $trans)
        <tr>
            <td>{{$trans['id']}}</td>
            <td>{{$trans['created_at']}}</td>
            <td>{{$trans['package_id']}}</td>
            <td>{{$trans['member_name']}}</td>
            <td>{{$trans['status']}}</td>
            <td>{{$trans['package_total']}}</td>
            <td><a href="{{action('TransactionController@downloadPDF', $trans['id'])}}" target="_blank">PRINT</a></td>

        </tr>
        @endforeach





    </tbody>
</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
