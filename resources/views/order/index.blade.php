<?php
// echo '<pre>';
// print_r($customer);
// echo '</pre>';
// exit;
 ?>

@extends('app')

@section('content')
<style>
  .bootstrap-select{
    width: 50% !important;
  }
</style>
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Orders</div>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.js"></script>


				<div class="panel-body">
					<form method="POST" action="{{action('OrderController@showList')}}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
            <select name="customer" data-live-search="true" data-live-search-style="startsWith" class="selectpicker">

              <?php
                  if($empty == 1){
                    $disabled = "disabled";
              ?>
                    <option value="">No Pending Orders</option>
              <?php
                } else {
                  $disabled = "";
               ?>
                <option value="">Search Name</option>
              @foreach($customer as $value)
								<option value="{{$value[0]->id}}">{{$value[0]->name}}</option>
							@endforeach
            <?php } ?>
            </select>
						<input class="add-to-cart btn btn-default" style="width: 35%;float: right;" type="submit" name="submit" {{$disabled}}>
					</form>


				</div>
			</div>
		</div>
	</div>
</div>
@endsection
