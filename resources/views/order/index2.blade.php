<?php

 ?>


@extends('app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Orders</div>

				<div class="panel-body">
          <form method="POST" action="{{action('SaleController@orderSale')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <td><input type="checkbox" class="checkAll" name="checkAll" /></td>
                        <td>Order ID</td>
                        <td>Date/Time</td>
                        <td>Package Purchased</td>
                        <td>Member Name</td>
                        <td>Customer Name</td>
                        <td>Package Price</td>
                        <td>Package Deposit</td>
                        <td>Order Status</td>
                        <td>TOTAL TO PAY</td>
                        <!-- <td>Action</td> -->
                    </tr>
                </thead>
                <tbody>
               @foreach($orderhistory as $order)
                    <tr>
                        <td><input type="checkbox" name="check[]" value="{{$order->id}}"/></td>
                        <td>{{$order->id}}</td>
                        <td>{{$order->created_at}}</td>
                        @foreach($packages as $package)
                        @if($order->package_id == $package['id'])
                        <td>{{$package['name']}}</td>
                        @endif
                        @endforeach
                        <td>{{$order->member_name}}</td>
                        @foreach($user_name as $user)
                        @if($order->cust_id == $user['id'])
                        <td>{{$user['name']}}</td>
                        @endif
                        @endforeach
                        <td>{{$order->package_price}}</td>
                        <td>{{$order->package_depo}}</td>
                        <td>{{$order->status}}</td>
                        <td>{{$order->package_total}}</td>
                        <!-- <td><a href="{{action('SaleController@orderSale', $order->id)}}"> <button class="add-to-cart btn btn-default" type="button">Pay</button></a></td> -->

                    </tr>
                @endforeach

                </tbody>
            </table>
            <input class="add-to-cart btn btn-default" type="submit" name="submit">
          </form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function () {
  $('.checkAll').on('click', function () {
    $(this).closest('table').find('tbody :checkbox')
      .prop('checked', this.checked)
      .closest('tr').toggleClass('selected', this.checked);
  });

  $('tbody :checkbox').on('click', function () {
    $(this).closest('tr').toggleClass('selected', this.checked); //Classe de seleção na row

    $(this).closest('table').find('.checkAll').prop('checked', ($(this).closest('table').find('tbody :checkbox:checked').length == $(this).closest('table').find('tbody :checkbox').length)); //Tira / coloca a seleção no .checkAll
  });
});
</script>
@endsection
