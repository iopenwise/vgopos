@extends('app')

@section('content')
<style>
  .bootstrap-select{
    width: 50% !important;
  }
</style>
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1" style="width:100%;">
			<div class="panel panel-default">
				<div class="panel-heading">Report</div>
				<div class="panel-body">
          <form method="POST" action="/downloadreport">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="payment_type" value="{{$payment_type}}">
            <input type="hidden" name="payment_status" value="{{$payment_status}}">
            <input type="hidden" name="date1" value="{{$date3}}">
            <input type="hidden" name="date2" value="{{$date4}}">
            <input type="submit" name="submit" class="btn btn-primary btn-md" style="float:right;" value="Export"><br>
          </form>
          <table class="table table-bordered">
            <tr>
              <th>id</th>
              <th>member_name</th>
              <th>cust_id</th>
              <th>package_id</th>
              <th>package_price</th>
              <th>package_depo</th>
              <th>status</th>
              <th>package_total</th>
              <th>amount_paid</th>
              <th>payment_method</th>
              <th>invoice_number</th>
            </tr>
            <?php $i = 1; ?>
            @foreach($orders as $value)
              <tr>
                <td>{{$i}}</td>
                <td>{{$value->member_name}}</td>
                <td>{{$value->cust_id}}</td>
                <td>{{$value->package_id}}</td>
                <td>{{$value->package_price}}</td>
                <td>{{$value->package_depo}}</td>
                <td>{{$value->status}}</td>
                <td>{{$value->package_total}}</td>
                <td>{{$value->amount_paid}}</td>
                <td>{{$value->payment_method}}</td>
                <td>{{$value->invoice_number}}</td>
              </tr>
              <?php $i++; ?>
            @endforeach
          </table>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
