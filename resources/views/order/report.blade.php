@extends('app')

@section('content')
<style>
  .bootstrap-select{
    width: 50% !important;
  }
</style>
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Orders</div>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.js"></script>


				<div class="panel-body">
					<form method="POST" action="/report2">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
            <select name="payment_status" data-live-search="true" data-live-search-style="startsWith" class="selectpicker">
                <option value="Paid">Paid</option>
								<option value="Pending">Pending Payment</option>
            </select>
            <p></p>
            <select name="payment_type" data-live-search="true" data-live-search-style="startsWith" class="selectpicker">
                <option value="Cash">Cash</option>
								<option value="Cheque">Cheque</option>
								<option value="Credit">Credit/Debit Card</option>
            </select>
            <p></p>
            <input style="width:50%;" class="form-control" type="text" name="daterange" value="">
            <p></p>
						<input class="add-to-cart btn btn-default" style="width: 35%;float: left;" type="submit" name="submit">
					</form>


				</div>
			</div>
		</div>
	</div>
</div>

<script>
$(function() {
  $('input[name="daterange"]').daterangepicker({
    opens: 'left',
    locale: {
      format: 'D/M/Y'
    }
  }, function(start, end, label) {
  });
});
</script>
@endsection
